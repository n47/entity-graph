INSERT INTO `building`
VALUES (1, 'Empire State Building');

INSERT INTO `apartment` (`id`, `type`, `building_id`)
VALUES (1, 'luxury', 1);

INSERT INTO `apartment` (`id`, `type`, `building_id`)
VALUES (2, 'regular', 1);

INSERT INTO `tenant` (`id`, `name`, `last_name`, `apartment_id`)
VALUES (1, 'John', 'Smith', 1);

INSERT INTO `tenant` (`id`, `name`, `last_name`, `apartment_id`)
VALUES (2, 'Sarah', 'Smith', 1);

INSERT INTO `tenant` (`id`, `name`, `last_name`, `apartment_id`)
VALUES (3, 'Williams', 'Johnson', 2);

INSERT INTO `tenant` (`id`, `name`, `last_name`, `apartment_id`)
VALUES (4, 'Mia', 'Johnson', 2);
