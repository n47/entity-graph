package com.north47.entitygraphdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntityGraphDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(EntityGraphDemoApplication.class, args);
    }

}
